const { distribution, liveVideo, o2o, supplyGood, im } = require('./index')

module.exports = {
	NODE_ENV: '"production"',
	ENV_CONFIG: '"prod"',
  DISTRIBUTION: distribution,
  LIVEVIDEO: liveVideo,
  O2O: o2o,
  SUPPLYGOOD: supplyGood,
  IM: im
}

# shopTNT电商系统-前端（PC端 商家PC端 管理端）

#### 介绍
shopTNT开源商城系统前端，包含买家PC端、商家PC端以及管理端。

### 演示地址

平台管理端：https://manager-bbc.shoptnt.cn 账号：admin/111111

店铺管理端：https://seller-bbc.shoptnt.cn 账号：ceshi/111111

商城PC页面：https://shop-bbc.shoptnt.cn 账号可自行注册

商城移动端，扫描如下二维码

### 开发/部署文档中心

https://docs.shoptnt.cn/docs/5.2.3

## 项目地址

后台API：https://gitee.com/bbc-se/api

前端ui：https://gitee.com/bbc-se/pc-ui

移动端：https://gitee.com/bbc-se/mobile-ui

配置中心：https://gitee.com/bbc-se/config

## 使用须知

1. 允许个人学习使用。
2. 允许用于学习、毕业设计等。
3. 禁止将本开源的代码和资源进行任何形式任何名义的出售。
4. 限制商业使用，如需[商业使用](http://www.shoptnt.cn)联系QQ：2025555598 或微信扫一扫加我微信。

![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/12261709021247_.pic.jpg?x-oss-process=style/300x300)

## 交流、反馈

### 推荐
官方QQ群：
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=RtRIL7WomrO79uDDDp4HihX_GH1xMIPD&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="ShopTNT开源交流群" title="ShopTNT开源交流群"></a> 766503360

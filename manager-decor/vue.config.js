const path = require('path')
const alias = require('ui-domain/alias')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  publicPath: alias.decor ? `/${alias.decor}/` : undefined,
  devServer: {
    port: 3004,
    disableHostCheck: true,
  },
  chainWebpack: config => {
    config.resolve.alias
      .set('~', resolve(''))
      .set('@', resolve('src'))
    config.module
      .rule('rpx')
      .test(/\.vue$/)
      .use('./src/utils/rpx-to-px-loader')
      .loader('./src/utils/rpx-to-px-loader')
      .end()
  }
}
